# README

## Fork of KitBoga's bank website

### Original readme:
> Here is the basic html structure for the bank i've been using. 
> If you're interested in creating some alternative "themes" you can pull this and create a separate
> css file in a folder like this: /themes/<yourtheme>/template.css 
>
> At this time I am not looking to update the html, though i may work with the community to do this in the future. 
>
> Your themes on separate branches will be reviewed by the community and may be used on stream so please only include code / images you have the rights to use and are able to give me (kitboga) the rights to broadcast and use. In other words, you need to be able to give me non-exclusive, perpetual, royalty-free license to use whatever you contribute here.   
>
> Thanks :)

### My changes: 
- added classes to checking, savings, and money market balances; and ids to paypal, direct deposit, and aol transaction values (in account.html).
- created js script that resets all balances to their default every 10 seconds (10,000 milliseconds), which could easily be changed to a different time interval if desired.

### The purpose of this fork is to troll refund scammers. When they try to "inspect element" to change the values of the cached webpage, they will be befuddled to find that the values automatically change back to what they were before, either right away or after a certain time interval.