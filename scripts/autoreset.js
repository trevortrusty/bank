// Cache HTML Balances
var checking = document.getElementsByClassName("checking");
var savings = document.getElementsByClassName("savings");
var market = document.getElementsByClassName("market");

// Cache HTML transaction values
var paypal = document.getElementById("paypal");
var direct = document.getElementById("direct");
var aol = document.getElementById("aol");

// Reset all balances and transations to their default values
window.onload = function() {
  setInterval(resetValues, 10000);
}

function resetValues()
{
	checking[0].innerHTML = "$15,123.21";
	checking[1].innerHTML = "$ 15,123.21";
	savings[0].innerHTML = "$59,141.93";
	savings[1].innerHTML = "$ 59,141.93";	
	market[0].innerHTML = "$507,277.36";
	market[1].innerHTML = "$ 507,277.36";
	paypal.innerHTML = "$ -227.09";
	direct.innerHTML = "$ 846.33";
	aol.innerHTML = "$ -46.17";
	console.log("Automically Fixed Account Values");
}